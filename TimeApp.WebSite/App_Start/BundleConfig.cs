﻿using BundleTransformer.Core.Bundles;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace TimeApp.WebSite
{
	public static class BundleConfig
	{
		// For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
		public static void RegisterBundles(BundleCollection bundles)
		{
#if DEBUG
			BundleTable.EnableOptimizations = false;
#else
			BundleTable.EnableOptimizations = true;
#endif
			bundles.Add(new CustomStyleBundle("~/Bundles/Stylesheets")
				.Include("~/Stylesheets/App/app.less"));

			bundles.Add((new ScriptBundle("~/Bundles/Scripts") { Orderer = new VendorScriptOrderer() })
				// Explicitly manage Vendor script order.
				.IncludeVendor(
					"modernizr.js", // Before all else so polyfills do not distort tests.
					"knockout.js",
					"lie.js",
					"underscore.js"
				)
				.IncludeDirectory("~/Scripts/PreApp", "*.js", true)
				.IncludeDirectory("~/Scripts/App", "*.js", true));
		}

		private class VendorScriptOrderer : IBundleOrderer
		{
			public System.Collections.Generic.IEnumerable<BundleFile> OrderFiles(BundleContext context, System.Collections.Generic.IEnumerable<BundleFile> files)
			{
				return files;
			}
		}

		private static Bundle IncludeVendor(this Bundle bundle, params string[] vendorFiles)
		{
			return bundle.Include(vendorFiles.Select(file => "~/Scripts/Vendor/" + file).ToArray());
		}
	}
}