﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using TimeApp.WebSite.Models;

namespace TimeApp.WebSite.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View(new Models.PageModel(ComponentInfo.AllComponents));
		}
	}
}