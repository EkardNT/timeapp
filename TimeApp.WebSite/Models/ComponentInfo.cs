﻿using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web.Hosting;
using System.Text;
namespace TimeApp.WebSite.Models
{
	public sealed class ComponentInfo
	{
		private const string ComponentsDirectory = "~/Views/Components/";
		private static readonly IEnumerable<ComponentInfo> allComponents =
			new DirectoryInfo(HostingEnvironment.MapPath(ComponentsDirectory))
			.EnumerateFiles()
			.Select(fileInfo => new ComponentInfo(fileInfo))
			.ToList(); // To force the load.

		public static IEnumerable<ComponentInfo> AllComponents { get { return allComponents; } }

		private ComponentInfo(FileInfo fileInfo)
		{
			Path = System.IO.Path.Combine(ComponentsDirectory, fileInfo.Name);
			ID = FileNameToComponent(System.IO.Path.GetFileNameWithoutExtension(fileInfo.FullName));
		}

		public string Path { get; private set; }
		public string ID { get; private set; }

		// Converts file name to component ID. Ex: FooBar => foo-bar-component.
		private string FileNameToComponent(string fileName)
		{
			var fileNameChars = fileName.ToCharArray();
			var builder = new StringBuilder();
			builder.Append(char.ToLowerInvariant(fileNameChars[0]));
			for (int i = 1; i < fileNameChars.Length; i++)
			{
				char c = fileNameChars[i];
				// The second part of this conditional preserves sequences of abbreviations,
				// ex: FooBarXYZ => foo-bar-xyz-component.
				if (char.IsUpper(c) && !char.IsUpper(fileNameChars[i - 1]))
					builder.Append('-');
				builder.Append(char.ToLowerInvariant(c));
			}
			builder.Append("-component");
			return builder.ToString();
		}
	}
}