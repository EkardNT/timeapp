﻿using System.Collections.Generic;

namespace TimeApp.WebSite.Models
{
	public class PageModel
	{
		public PageModel(IEnumerable<ComponentInfo> components)
		{
			Components = components;
		}

		public IEnumerable<ComponentInfo> Components { get; private set; }
	}
}