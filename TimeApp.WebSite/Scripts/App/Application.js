﻿namespace('App', function (define, require, using) {
	'use strict';

	var Initialization = require('App.Lib:Initialization');
	var ApplicationState = require('App.State:Application');

	define('Application', function () {
		// Independent observables declared here.
		var vm = {
			title: ko.pureComputed(function () { return ApplicationState().title(); }),
			timerVisible: ko.pureComputed(function () { return ApplicationState().activeTool() === ApplicationState().Tool.Timer; }),
			stopwatchVisible: ko.pureComputed(function () { return ApplicationState().activeTool() === ApplicationState().Tool.Stopwatch; }),
			clockVisible: ko.pureComputed(function () { return ApplicationState().activeTool() === ApplicationState().Tool.Clock; }),
			initializing: ko.observable(true),
			initializationErrors: ko.observableArray()
		};
		// Dependent observables declared here.
		vm.initializationFailed = ko.pureComputed(function () { return vm.initializationErrors().length > 0; });

		var htmlRoot = document.getElementsByTagName('html')[0];

		Initialization().registerInitializer(function () {
			ko.applyBindings(vm, htmlRoot);
			return Promise.resolve();
		});

		window.addEventListener('load', function () {
			Initialization().initialize()
				.catch(function (errors) {
					vm.initializationErrors(errors);
				})
				.finally(function () {
					vm.initializing(false);
				});
		});
	});
});