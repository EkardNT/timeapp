﻿namespace('App.Bindings', function (define, require, using) {
	'use strict';

	var StateManager = require('App.Lib:StateManager');

	define('StateHref', function () {
		function init(element, valueAccessor, allBindings, __deprecated, bindingContext) {
			if (element.nodeName !== 'A')
				throw new Error('The \'stateHref\' binding can only be applied to HTML anchor elements.');

			var onClick = function (e) {
				e.preventDefault();
				var path = ko.unwrap(valueAccessor());
				StateManager().navigate(path);
			};

			element.addEventListener('click', onClick, false);
			ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
				element.removeEventListener('click', onClick, false);
			});
		}

		function update(element, valueAccessor, allBindings, __deprecated, bindingContext) {
			if (element.nodeName !== 'A')
				throw new Error('The \'stateHref\' binding can only be applied to HTML anchor elements.');
			element.href = ko.unwrap(valueAccessor());
		}

		ko.bindingHandlers.stateHref = {
			init: init,
			update: update
		};
	});
});