﻿namespace('App.Components', function (define, require, using) {
	'use strict';

	var StateManager = require('App.Lib:StateManager');
	var ApplicationState = require('App.State:Application');

	define('ClockTool', function () {
		var createViewModel = function () {
			var vm = {};

			return vm;
		};

		StateManager().defineState('/clock/',
			function enterClockState() {
				console.log('enterClockState');
				ApplicationState().activeTool(ApplicationState().Tool.Clock);
			},
			function exitClockState() {
				console.log('exitClockState');
			}
		);

		ko.components.register('clock-tool-component', {
			viewModel: { createViewModel: createViewModel },
			template: { element: document.getElementById('clock-tool-component') }
		});
	});
});