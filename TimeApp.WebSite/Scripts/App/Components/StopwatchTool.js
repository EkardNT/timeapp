﻿namespace('App.Components', function (define, require, using) {
	'use strict';

	var StateManager = require('App.Lib:StateManager');
	var ApplicationState = require('App.State:Application');

	define('StopwatchTool', function () {
		var createViewModel = function () {
			var vm = {};

			return vm;
		};

		StateManager().defineState('/stopwatch/',
			function enterStopwatchState() {
				console.log('enterStopwatchState');
				ApplicationState().activeTool(ApplicationState().Tool.Stopwatch);
			},
			function exitStopwatchState() {
				console.log('exitStopwatchState');
			}
		);

		ko.components.register('stopwatch-tool-component', {
			viewModel: { createViewModel: createViewModel },
			template: { element: document.getElementById('stopwatch-tool-component') }
		});
	});
});