﻿namespace('App.Components', function (define, require, using) {
	'use strict';

	var StateManager = require('App.Lib:StateManager');
	var ApplicationState = require('App.State:Application');

	define('TimerTool', function () {
		var createViewModel = function () {
			var vm = {};

			return vm;
		};

		StateManager().defineState('/timer/',
			function enterTimerState() {
				console.log('enterTimerState');
				ApplicationState().activeTool(ApplicationState().Tool.Timer);
			},
			function exitTimerState() {
				console.log('exitTimerState');
			}
		);

		ko.components.register('timer-tool-component', {
			viewModel: { createViewModel: createViewModel },
			template: { element: document.getElementById('timer-tool-component') }
		});
	});
});