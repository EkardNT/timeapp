﻿namespace('App.Components', function (define, require, using) {
	'use strict';

	var StateManager = require('App.Lib:StateManager');
	var ApplicationState = require('App.State:Application');

	define('ToolSelect', function () {
		var createViewModel = function () {
			var vm = {
				'timerMajor': ko.pureComputed(function () { return ApplicationState().activeTool() === ApplicationState().Tool.Timer; }),
				'stopwatchMajor': ko.pureComputed(function () { return ApplicationState().activeTool() === ApplicationState().Tool.Stopwatch; }),
				'clockMajor': ko.pureComputed(function () { return ApplicationState().activeTool() === ApplicationState().Tool.Clock; }),
			};

			return vm;
		};

		ko.components.register('tool-select-component', {
			viewModel: { createViewModel: createViewModel },
			template: { element: document.getElementById('tool-select-component') }
		});
	});
});