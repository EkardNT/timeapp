﻿namespace('App.Lib', function (define, require, using) {
	'use strict';

	define('Disposable', function () {
		// Input:
		//	disposer {Function} - A function that will be called at most once, when the Disposable's
		//		dispose() method is called.
		// Output:
		//	{Object} - A Disposable object is an object with a dispose() method which can be called
		//		to perform some clean up action, release a subscription, etc.
		return function (disposer) {
			return {
				dispose: _.once(disposer)
			};
		};
	});
});