﻿namespace('App.Lib', function (define, require, using) {
	'use strict';

	define('Events', function () {
		var events = {};

		events.app = {};
		events.app.initializing = 'app:initializing';

		return events;
	});
});