﻿namespace('App.Lib', function (define, require, using) {
	'use strict';

	define('Initialization', function () {
		// Externally visible API.
		var initialization = {};
		// Simple array of registered initializer functions.
		var initializers = [];
		// Flag indicating whether initialize() function has been called.
		var initialized;

		// Registers an initializer function. No ordering is imposed on initializers, so this
		// function should not depend on executing before or after any other initializer.
		// Inputs:
		//	initializer {Function} - A function which will be called when initialization occurs. This
		//		function will receive unchanged all arguments from the call to the Initialization::initialize
		//		function. Should return a promise which is either resolved with no arguments or rejected
		//		with an Error or array of Errors describing the cause(s) of the failure.
		initialization.registerInitializer = function (initializer) {
			if (initialized)
				throw new Error('Cannot register an initializer after initialization has already occurred.');
			if (!_.isFunction(initializer))
				throw new Error('An initializer must be a function.');
			initializers.push(initializer);
		};

		// Runs all initializers. Any arguments passed to this function are passed unchanged to
		// each initializer function.
		// Outputs:
		//	{Promise} A promise representing the initialization. Every initializer is called. If
		//		any initializers reject, this return promise also rejects with an array of Errors
		//		composed of the union of all initializer rejection Errors. Otherwise, if all 
		//		initializers resolve successfully, then this return promise also resolves.
		initialization.initialize = function () {
			if (initialized)
				throw new Error('Cannot call initialize twice.');
			initialized = true;
			return new Promise(function (resolve, reject) {
				var initializersRemaining = initializers.length;
				var errors = [];
				// Apply arguments to each initializer. Do not provide a 'this' context for 
				// initializers, if clients really want one they can bind() the initializers.
				initializers.forEach(function (initializer) {
					initializer.apply(undefined, arguments)
						.catch(function (errorOrErrors) {
							if (errorOrErrors instanceof Error)
								errors.push(errorOrErrors);
							else if (_.isArray(errorOrErrors)) {
								for (var i = 0; i < errorOrErrors.length; i++)
									errors.push(errorOrErrors[i]);
							}
						})
						.finally(function () {
							initializersRemaining--;
							// If there are no initializers still executing,
							// we can settle the initialization promise.
							if (initializersRemaining === 0) {
								if (errors.length === 0)
									resolve();
								else
									reject(errors);
							}
						}
					);
				});
			});
		};

		return initialization;
	});
});