﻿namespace('App.Lib', function (define, require, using) {
	'use strict';

	var Initialization = require('App.Lib:Initialization');

	define('StateManager', function () {
		// External API.
		var stateManager = {};
		// Simple array of all defined states.
		var states = [];
		// Record the current active path to guard against duplicate state updates.
		var currentActivePath = null;
		// Flag indicating whether the state manager has been initialized.
		var initialized = false;

		// Defines a new state.
		// Inputs:
		//	path {String} - The absolute path which defines the state, for example '/foo/bar/baz'.
		//		Note that paths ending with a slash is a different, deeper state than the same
		//		path without a slash.
		//	onEnter {Function} - The function to call (accepting no parameters) when the state is entered.
		//	onExit {Function} - The function to call (accepting no parameters) when the state is exited.
		stateManager.defineState = function (path, onEnter, onExit) {
			if (initialized)
				throw new Error('Cannot define a new state once the StateManager has been initialized.');
			states.push(new State(
				splitPathComponents(path),
				_.isFunction(onEnter) ? onEnter : null,
				_.isFunction(onExit) ? onExit : null));
		};

		// Transitions to a new state, preserving the current state in
		// the history stack.
		// Inputs:
		//	path {String} - The absolute path which defines the state to transition to.
		stateManager.navigate = function (path) {
			if (!initialized)
				throw new Error('Cannot navigate until the StateManager has been initialized.');
			window.history.pushState(null, null, path);
			window.setTimeout(updateStates, 0);
		};

		// Transitions to a new state, replacing the current state in
		// the history stack.
		// Inputs:
		//	path {String} - The absolute path which defines the state to transition to.
		stateManager.replace = function (path) {
			if (!initialized)
				throw new Error('Cannot replace until the StateManager has been initialized.');
			window.history.replaceState(null, null, path);
			window.setTimeout(updateStates, 0);
		};

		// Transitions from the current state to the next state, if one exists. Otherwise
		// has no effect.
		stateManager.forward = function () {
			if (!initialized)
				throw new Error('Cannot move forward until the StateManager has been initialized.');
			window.history.forward();
		};

		// Transitions from the current state to the previous state, if one exists.
		// Otherwise has no effect.
		stateManager.back = function () {
			if (!initialized)
				throw new Error('Cannot move back until the StateManager has been initialized.');
			window.history.back();
		};

		function onPopState() {
			window.setTimeout(updateStates, 0);
		}

		function updateStates() {
			// Always get new path from window location.
			var newPath = window.location.pathname;
			// Guard against duplicate updates.
			if (currentActivePath === newPath)
				return;
			currentActivePath = newPath;
			// Build lists of states which need to be deactivated and activated.
			var pathComponents = splitPathComponents(newPath);
			var deactivating = [], activating = [];
			for (var i = 0; i < states.length; i++) {
				var state = states[i];
				var activated = state.activatedBy(pathComponents);
				var active = state.active;
				// Only two of the four possible combinations result in state transitions.
				if (activated && !active)
					activating.push(state);
				else if (!activated && active)
					deactivating.push(state);
			}
			// Deactivate, then activate
			for (var i = 0; i < deactivating.length; i++) {
				var state = deactivating[i];
				if (state.onExit)
					state.onExit();
				state.active = false;
			}
			for (var i = 0; i < activating.length; i++) {
				var state = activating[i];
				if (state.onEnter)
					state.onEnter();
				state.active = true;
			}
		}

		Initialization().registerInitializer(function () {
			initialized = true;
			window.setTimeout(updateStates, 0);
			return Promise.resolve();
		});

		return stateManager;
	});

	function splitPathComponents(path) {
		if (!_.isString(path))
			throw new Exception('Path must be a string.');
		if (path.length === 0)
			throw new Exception('Path must be non-empty.');
		if (path.charAt(0) !== '/')
			throw new Exception('Path must start with a \'/\'.');
		var buffer = '';
		var components = [];
		for (var i = 0; i < path.length; i++) {
			var char = path.charAt(i);
			if (char === '/') {
				if (buffer.length > 0)
					components.push(buffer);
				components.push('/');
				buffer = '';
			}
			else {
				buffer += char;
			}

		}
		if (buffer.length > 0)
			components.push(buffer);
		return components;
	}

	function State(components, onEnter, onExit) {
		var self = this;

		self.components = components;
		self.onEnter = onEnter;
		self.onExit = onExit;
		self.active = false;

		self.activatedBy = function (otherComponents) {
			if (self.components.length > otherComponents.length)
				return false;
			for (var i = self.components.length - 1; i >= 0; i--) {
				if (self.components[i] !== otherComponents[i])
					return false;
			}
			return true;
		};
	}
});