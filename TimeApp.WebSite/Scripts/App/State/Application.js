﻿namespace('App.State', function (define, require, using) {
	'use strict';

	// Defines state common across the entire application.
	define('Application', function () {
		var state = {};

		state.Tool = {
			Timer: 'Tool.Timer',
			Stopwatch: 'Tool.Stopwatch',
			Clock: 'Tool.Clock'
		};
		state.title = ko.observable('TimeApp');
		state.activeTool = ko.observable();

		return state;
	});
});