﻿(function IgnoreBindings() {
	'use strict';
	ko.bindingHandlers.ignoreBindings = {
		init: function () {
			return { controlsDescendantBindings: true };
		}
	};
})();