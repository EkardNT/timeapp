﻿(function Namespacing(window) {
	"use strict";

	// Constants
	var NAMESPACE_SEPARATOR = ".";
	var NAMESPACE_MODULE_SEPARATOR = ":";
	var State = {
		STUB: "STUB", DEFINED: "DEFINED", ACTIVE: "ACTIVE"
	}

	// Module definitions. Each key is the module's full name, each value is a Module object.
	var modules = {};

	// Whether the defineNamespace function is currently executing.
	var inNamespaceDeclaration = false;
	// Whether a module factory is current executing.
	var inModuleFactory = false;
	// string[], elements are required modules' full names.
	var requireDeclarations = null;
	// string[], elements are usinged modules' full names.
	var usingDeclarations = null;
	// string[], elements are declared modules' full names.
	var moduleDeclarations = null;

	// Defines a namespace. 'name' is a non-empty string.
	function defineNamespace(name, factory) {
		if (inNamespaceDeclaration)
			throw new Error('Cannot declare nested namespaces (yet?).');
		if (!_.isString(name))
			throw new Error("Namespace name must be a string.");

		requireDeclarations = [];
		usingDeclarations = [];
		moduleDeclarations = [];

		inNamespaceDeclaration = true;
		// Allow clients to define and require modules.
		factory(
			function define(moduleName, factory) { defineModule(name, moduleName, factory); },
			function require(fullName) { return requireModule(fullName); },
			function using(fullName) { return usingModule(fullName); });
		inNamespaceDeclaration = false;

		ensureStubsExist(requireDeclarations);
		ensureStubsExist(usingDeclarations);
		checkCircularDependencies(moduleDeclarations);
		registerAsDependants(moduleDeclarations);
		moduleDeclarations.forEach(function (fullName) {
			tryActivateModule(modules[fullName]);
		});

		usingDeclarations = null;
		requireDeclarations = null;
		moduleDeclarations = null;
	}

	// Defines a Module.
	function defineModule(namespaceName, moduleName, factory) {
		if (!inNamespaceDeclaration)
			throw new Error('Can only define a module inside a namespace declaration.');
		if (!_.isFunction(factory))
			throw new Error('Must provide a factory function when defining a module.');

		var fullName = getFullName(namespaceName, moduleName);

		// Get module. If it does not exist, create a new one.
		var module = modules[fullName] = modules.hasOwnProperty(fullName) ? modules[fullName] : new Module(fullName);

		// If the module isn't a stub, that means it has already been defined.
		if (module.state !== State.STUB)
			throw new Error('Module "' + fullName + '" has already been defined.');

		// Fill the module's fields and add it to the modules set.
		module.state = State.DEFINED;
		module.factory = factory;
		module.dependencies = requireDeclarations;

		// Record the module's name so we know to activate it after the namespace declaration is finished.
		moduleDeclarations.push(fullName);
	}

	// Returns a module reference, which is a function which when called inside a Module factory
	// will return the value of the required module.
	function requireModule(fullName) {
		if (inModuleFactory)
			throw new Error('Cannot require modules inside a module factory function.');
		if (!inNamespaceDeclaration)
			throw new Error('Can only require modules inside a namespace declaration.');

		requireDeclarations.push(fullName);

		var didResolveModule = false;
		var resolvedModule = undefined;

		function resolveModule() {
			if (inNamespaceDeclaration)
				throw new Error('Cannot access the value of a require declaration while in a namespace declaration.');
			if (!modules.hasOwnProperty(fullName))
				throw new Error('Module "' + fullName + '" is required but has not been defined.');
			var module = modules[fullName];
			if (module.state === State.STUB)
				throw new Error('Module "' + fullName + '" is required but has only been referenced as a dependency, not defined.');
			if (module.state !== State.ACTIVE)
				throw new Error('Module "' + fullName + '" is required but has not been activated.');
			resolvedModule = module.resolvedModule;
		}

		function getResolvedModule() {
			if (!didResolveModule) {
				resolveModule();
				didResolveModule = true;
			}
			return resolvedModule;
		}

		return getResolvedModule;
	}

	// Returns a module reference, which is a function which when called inside a Module (but not
	// in the factory itself) will return the value of the used module.
	function usingModule(fullName) {
		if (inModuleFactory)
			throw new Error('Cannot declare a using binding inside a module factory function.');
		if (!inNamespaceDeclaration)
			throw new Error('Can only require modules inside a namespace declaration.');

		usingDeclarations.push(fullName);

		var resolvedModule = null;

		function resolveModule() {
			if (inNamespaceDeclaration)
				throw new Error('Cannot access the value of a using declaration inside a namespace declaration.');
			if (inModuleFactory)
				throw new Error('Cannot access a using declaration inside a module factory.');
			if (!modules.hasOwnProperty(fullName))
				throw new Error('Module "' + fullName + '" is the target of a using declaration but has not been defined.');
			var module = modules[fullName];
			if (module.state === State.STUB)
				throw new Error('Module "' + fullName + '" is the target of a using declaration but has only been referenced as a dependency, not defined.');
			if (module.state !== State.ACTIVE)
				throw new Error('Module "' + fullName + '" is the target of a using declaration but has not been activated.');
			resolvedModule = module.resolvedModule;
		}

		function getResolvedModule() {
			if (resolvedModule === null)
				resolveModule();
			return resolvedModule;
		}

		return getResolvedModule;
	}

	// Throws if any of the modules whose full names are inside the fullNames array is part of
	// a circular dependency.
	function checkCircularDependencies(fullNames) {
		var circularDependencyModules = [];
		fullNames.forEach(function (fullName) {
			var module = modules[fullName];
			if (isPartOfCircularDependency(module)) {
				circularDependencyModules.push(module);
			}
		});
		if (circularDependencyModules.length > 0) {
			var names = '';
			circularDependencyModules.forEach(function (module) {
				module.state = State.STUB;
				module.factory = null;
				module.dependencies = null;
				names += '"' + module.fullName + '", ';
			});
			throw new Error('Modules ' + names.substring(0, names.length - 2) + ' were involved in a circular dependency.');
		}
	}

	// Returns true if the given module is part of a circular dependency.
	function isPartOfCircularDependency(module) {
		return detectCircularDependency(module.fullName, module.dependencies);
	}

	// Returns true if a circular dependency is detected. A circular dependency exists if a module
	// depends on itself of any of its dependencies depend on it.
	function detectCircularDependency(rootFullName, dependencies) {
		if (dependencies.indexOf(rootFullName) !== -1)
			return true;
		return dependencies.indexOf(rootFullName) !== -1 || _.some(dependencies, function (fullName) {
			var module = modules[fullName];
			return _.isArray(module.dependencies) && detectCircularDependency(rootFullName, module.dependencies);
		});
	}

	// Ensures that at least stub modules exist for every given full name.
	function ensureStubsExist(fullNames) {
		fullNames.forEach(function (fullName) {
			if (!modules.hasOwnProperty(fullName))
				modules[fullName] = new Module(fullName);
		});
	}

	// Registers every module whose name is in the given fullNames array as a
	// dependant of their dependencies.
	function registerAsDependants(fullNames) {
		fullNames.forEach(function (fullName) {
			var module = modules[fullName];
			module.dependencies.forEach(function (dependencyFullName) {
				var dependency = modules[dependencyFullName];
				dependency.dependants.push(fullName);
			});
		});
	}

	// Trys to activate a module.
	function tryActivateModule(module) {
		// STUB modules cannot be activated, and ACTIVE modules do not need to be.
		if (module.state !== State.DEFINED)
			return;
		// We can only activate if all dependencies are ACTIVE.
		if (_.some(module.dependencies, function (dependencyName) {
			return modules[dependencyName].state !== State.ACTIVE
		}))
			return;
		// Invoke the factory function to resolve the module.
		inModuleFactory = true;
		module.resolvedModule = module.factory(); // Note: factory can return anything, even null or undefined.
		inModuleFactory = false;
		module.state = State.ACTIVE;
		// This module being activated might cause dependant modules to be able to activate.
		module.dependants.forEach(function (dependantName) {
			var dependant = modules[dependantName];
			tryActivateModule(dependant);
		});
	}

	// Concatenates namespace name and module name into full name.
	function getFullName(namespaceName, moduleName) {
		if (!_.isString(namespaceName) || !_.isString(moduleName))
			throw new Error("Both namespaceName and moduleName must be strings.");
		return namespaceName + NAMESPACE_MODULE_SEPARATOR + moduleName;
	}

	function Module(fullName) {
		// Concatenation of namespace name and module name.
		this.fullName = fullName;
		// String[] whose elements are the full names of the modules on which this module depends.
		this.dependencies = null;
		// Function which takes in an object containing resolved dependencies and returns a non-null
		// object which is the resolved module.
		this.factory = null;
		// String[] whose elements are the full names of the modules which depend on this module.
		this.dependants = [];
		// The resolved module returned by the factory function, or null if the factory has not been
		// called yet because its dependencies have not been satisfied.
		this.resolvedModule = null;
		// Module state.
		this.state = State.STUB;
	}

	// Export.
	window.namespace = defineNamespace;
})(window);