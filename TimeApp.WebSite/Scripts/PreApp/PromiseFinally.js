﻿(function PromiseFinally() {
	'use strict';

	if (window.hasOwnProperty('Promise') && !window.Promise.prototype.hasOwnProperty('finally')) {
		window.Promise.prototype['finally'] = function (callback) {
			if (!this instanceof window.Promise)
				throw new Error('Called finally with a "this" context which is not a Promise.');
			var called = false;
			return this.then(
				function () {
					if (called)
						return;
					called = true;
					callback.apply(undefined, arguments);
				},
				function () {
					if (called)
						return;
					called = true;
					callback.apply(undefined, arguments);
				}
			);
		};
	}
})();